package com.mycompany.flightticketmanagement.domain;

import java.time.ZonedDateTime;

/**
 *
 * @author boom
 */
public class Flight {
    
         private String flightNumber;
         private AirCraftType aircraftType;
         private AirLine operator;
         private ZonedDateTime departureTime;
         private ZonedDateTime arrivalTime;
         private Airport fromAirport;
         private Airport toAirport;
         private boolean inBound;
         private int remainingSeats;
         private String departureTerminal;
         private String arrivalTerminal;
         private int numberOfTerminal;
         private FareRate fare;

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setAircraftType(AirCraftType aircraftType) {
        this.aircraftType = aircraftType;
    }

    public void setOperator(AirLine operator) {
        this.operator = operator;
    }

    public void setDepartureTime(ZonedDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public void setArrivalTime(ZonedDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setFromAirport(Airport fromAirport) {
        this.fromAirport = fromAirport;
    }

    public void setToAirport(Airport toAirport) {
        this.toAirport = toAirport;
    }

    public void setInBound(boolean inBound) {
        this.inBound = inBound;
    }

    public void setRemainingSeats(int remainingSeats) {
        this.remainingSeats = remainingSeats;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public void setNumberOfTerminal(int numberOfTerminal) {
        this.numberOfTerminal = numberOfTerminal;
    }

    public void setFare(FareRate fare) {
        this.fare = fare;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public AirCraftType getAircraftType() {
        return aircraftType;
    }

    public AirLine getOperator() {
        return operator;
    }

    public ZonedDateTime getDepartureTime() {
        return departureTime;
    }

    public ZonedDateTime getArrivalTime() {
        return arrivalTime;
    }

    public Airport getFromAirport() {
        return fromAirport;
    }

    public Airport getToAirport() {
        return toAirport;
    }

    public boolean isInBound() {
        return inBound;
    }

    public int getRemainingSeats() {
        return remainingSeats;
    }

    public String getDepartureTerminal() {
        return departureTerminal;
    }

    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    public int getNumberOfTerminal() {
        return numberOfTerminal;
    }

    public FareRate getFare() {
        return fare;
    }
         
    
}
