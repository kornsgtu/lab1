/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightticketmanagement.domain;

/**
 *
 * @author SONY
 */
public enum Airport {
    BKK("Suvarnabhumi Airport","Bangkok(Bang Phli,Samut Prakan)","TH"),
    DMK("Don Mueang International Airport","Bangkok","TH"),
    HKT("Phuket International Airport","Phuket","TH"),
    CNX("Chiang Mai International Airport","Chiang Mai","TH"),
    HDY("Hat yai International Airport","Songkhla","TH"),
    CEI("Mae Fah Luang Chiang Rai International Airport","Chiang Rai","TH");
    
    private final String airportName;
    private final String city;    
    private final String country;
    
    private Airport(String airportName,String city,String country){
        this.airportName = airportName;
        this.city = city;
        this.country = country;
    }
    public String getAirportName(){
        return airportName;
    }
    public String getcityName(){
        return city;
    }
    public String getcountryName(){
        return country;
    }
}
